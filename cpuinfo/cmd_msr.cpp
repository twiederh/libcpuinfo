// SPDX-License-Identifier: GPL-2.0-or-later
// Copyright 2023 Tim Wiederhake

#include "cpuinfo.hpp"

#include <getopt.h>
#include <iostream>
#include <libcpuinfo/arch/x86.h>

static constexpr auto usage_string = std::string_view {
    "Usage: cpuinfo msr [OPTIONS] COMMAND\n"
    "Read and write machine specific registers.\n"
    "\n"
    "Options:\n"
    "  -h, --help\n"
    "        Display this help and exit.\n"
    "\n"
    "Available commands:\n"
    "  [read] INDEX\n"
    "        Read data from specified msr.\n"
    "  write INDEX VALUE [VALUE]\n"
    "        Write data to specified msr. If one VALUE is given, it is\n"
    "        written as 64-bit value to the specified msr. If two VALUES are\n"
    "        given, the lower 32 bit of the first value are written to the\n"
    "        lower 32 bit of the msr and the lower 32 bit of the second value\n"
    "        are written to the upper 32 bit of the msr.\n"
};

static int cmd_msr_help(Output& output) {
    output << usage_string;
    return 0;
}

static int cmd_msr_read(Output& output, char* argv[]) {
    if (argv[0] == nullptr || argv[1] != nullptr) {
        cmd_msr_help(output);
        return 1;
    }

    auto index = uint32_t {};

    try {
        index = std::stoul(argv[0], nullptr, 0);
    } catch (...) {
        cmd_msr_help(output);
        return 1;
    }

    auto eax = uint32_t {};
    auto edx = uint32_t {};
    if (cpuinfo_x86_msr_read(index, &eax, &edx) != 0) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    auto map = Map {};
    map.add_value("eax", eax);
    map.add_value("edx", edx);

    output << map << '\n';
    return 0;
}

static int cmd_msr_write(Output& output, char* argv[]) {
    if (argv[0] == nullptr || argv[1] == nullptr) {
        cmd_msr_help(output);
        return 1;
    }

    auto index = uint32_t {};
    auto eax = uint32_t {};
    auto edx = uint32_t {};

    try {
        index = std::stoul(argv[0], nullptr, 0);
        auto value1 = std::stoull(argv[1], nullptr, 0);

        if (argv[2] == nullptr) {
            eax = 0xffffffffUL & (value1 >> 0UL);
            edx = 0xffffffffUL & (value1 >> 32UL);
        } else {
            if (argv[3] != nullptr) {
                cmd_msr_help(output);
                return 1;
            }

            auto value2 = std::stoul(argv[2], nullptr, 0);
            eax = 0xffffffffUL & value1;
            edx = 0xffffffffUL & value2;
        }
    } catch (...) {
        cmd_msr_help(output);
        return 1;
    }

    if (cpuinfo_x86_msr_write(index, eax, edx) != 0) {
        std::cerr << "Error: " << cpuinfo_error_get() << '\n';
        return 1;
    }

    return 0;
}

int cmd_msr(Output& output, int argc, char* argv[]) {
    auto opt_short = "+:h";
    option opt_long[] = {
        { "help", no_argument, nullptr, 'h' },
        { nullptr, 0, nullptr, 0 }
    };

    auto arg = -1;
    auto idx = -1;

    // NOLINTNEXTLINE: getopt_long is not thread safe
    while ((arg = getopt_long(argc, argv, opt_short, opt_long, &idx)) != -1) {
        switch (arg) {
        case 'h':
            return cmd_msr_help(output);

        case ':':
            std::cerr << "Error: Missing argument for option\n";
            return 1;

        default:
            std::cerr << "Error: Invalid option\n";
            return 1;
        }
    }

    if (argc == optind) {
        cmd_msr_help(output);
        return -1;
    }

    auto cmd = std::string { argv[optind] };

    if (cmd == "read") {
        return cmd_msr_read(output, argv + optind + 1);
    }

    if (cmd == "write") {
        return cmd_msr_write(output, argv + optind + 1);
    }

    return cmd_msr_read(output, argv + optind);
}
