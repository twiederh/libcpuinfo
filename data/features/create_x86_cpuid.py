#!/bin/env python3

import argparse
import os


def parse_args():
    parser = argparse.ArgumentParser(
        description="Create templates for x86 CPU feature XML files.")
    parser.add_argument(
        "register",
        help="Output register",
        choices=["eax", "ebx", "ecx", "edx"])
    parser.add_argument("EAX", help="cpuid level / eax in")
    parser.add_argument("ECX", help="cpuid branch / ecx in", nargs="?")
    args = parser.parse_args()
    args.EAX = int(args.EAX, 0)
    args.ECX = None if args.ECX is None else int(args.ECX, 0)
    return args


def filename(args):
    name = "x86_cpuid_{:08x}_{:08x}_{:s}.xml".format(
        args.EAX,
        0 if args.ECX is None else args.ECX,
        args.register)
    return os.path.join(os.path.dirname(__file__), name)


def write_template(f, args):
    template = {
        True: "                <{0}>0x{1:08x}</{0}>\n",
        False: "                <{0}/>\n",
    }

    f.write("<?xml version=\"1.0\"?>\n")
    f.write("<features>\n")
    for index in range(0, 32):
        f.write("    <!--\n")
        f.write("    <feature>\n")
        f.write("        <name></name>\n")
        f.write("        <aliases/>\n")
        f.write("        <description></description>\n")
        f.write("        <family>x86</family>\n")
        f.write("        <features/>\n")
        f.write("        <extra>\n")
        f.write("            <x86-cpuid>\n")
        f.write("                <eax-in>0x{:08x}</eax-in>\n".format(args.EAX))
        f.write(template[args.ECX is not None].format("ecx-in", args.ECX))
        for reg in ("eax", "ebx", "ecx", "edx"):
            f.write(template[args.register == reg].format(reg, 1 << index))
        f.write("            </x86-cpuid>\n")
        f.write("        </extra>\n")
        f.write("    </feature>\n")
        f.write("    -->\n")
    f.write("</features>\n")


def main():
    args = parse_args()
    with open(filename(args), "tw") as f:
        write_template(f, args)


if __name__ == "__main__":
    main()
