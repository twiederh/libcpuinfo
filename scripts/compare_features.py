#!/bin/env python3

import argparse
import json
import lxml.etree
import os
import pycpuinfo
import re


def load_libcpuinfo():
    x86 = pycpuinfo.Family.find("x86")

    names = dict()
    for feature in pycpuinfo.features():
        # skip non-x86 features
        if feature.family() != x86:
            continue

        name = feature.name()
        data = feature.extra_x86_cpuid() or feature.extra_x86_msr()

        # skip compound features
        if not data:
            continue

        names[name] = data
    return names


def load_libvirt(basedir):
    filename = os.path.join(basedir, "src/cpu_map/x86_features.xml")
    parser = lxml.etree.XMLParser(remove_comments=True, remove_blank_text=True)
    with open(filename, "tr") as f:
        doc = lxml.etree.parse(f, parser=parser)
    names = dict()
    for node in doc.getroot():
        name = node.attrib["name"]
        feature = pycpuinfo.Feature.find(name, "qemu")
        if feature:
            name = feature.name()

        cpuid = node.find("cpuid")
        if cpuid is not None:
            ecx_in = int(cpuid.attrib.get("ecx_in", "0x656e6f4e"), 0)
            eax_in = int(cpuid.attrib.get("eax_in", "0"), 0)
            eax = int(cpuid.attrib.get("eax", "0"), 0)
            ebx = int(cpuid.attrib.get("ebx", "0"), 0)
            ecx = int(cpuid.attrib.get("ecx", "0"), 0)
            edx = int(cpuid.attrib.get("edx", "0"), 0)
            names[name] = (eax_in, ecx_in, eax, ebx, ecx, edx)

        msr = node.find("msr")
        if msr is not None:
            index = int(msr.attrib.get("index", "0"), 0)
            eax = int(msr.attrib.get("eax", "0"), 0)
            edx = int(msr.attrib.get("edx", "0"), 0)
            names[name] = (index, eax, edx)

    return names


def qemu_get_constants(basedir):
    pattern_define = re.compile("^#define\\s+(\\S+)\\s+(.*)$")

    headers = (
        "include/standard-headers/asm-x86/kvm_para.h",
        "target/i386/cpu.h",
    )

    constants = {"true": "1"}
    for header in headers:
        with open(os.path.join(basedir, header), "tr") as f:
            for line in f.readlines():
                match = pattern_define.match(line)
                if match:
                    constants[match.group(1)] = match.group(2)
    return constants


def qemu_parse_file(basedir):
    filename = os.path.join(basedir, "target/i386/cpu.c")
    pattern_comment = re.compile("/\\*.*?\\*/")
    marker_begin = "FeatureWordInfo feature_word_info[FEATURE_WORDS] = {\n"
    marker_end = "};\n"

    with open(filename, "tr") as f:
        # skip until begin marker
        while True:
            line = f.readline()
            if not line:
                exit("begin marker not found in cpu.c")
            if line == marker_begin:
                break

        # read until end marker
        while True:
            line = f.readline()
            if not line:
                exit("end marker not found in cpu.c")
            if line == marker_end:
                break

            # remove comments and white space
            line = re.sub(pattern_comment, "", line).strip()
            yield line


def qemu_parse_lines(lines):
    state_waiting_for_type = 1
    state_waiting_for_names = 2
    state_read_names = 3
    state_waiting_for_query = 4
    state_read_query = 5

    pattern_type = re.compile("^\\.type\\s*=\\s*(.+)$")
    pattern_names = re.compile("^\\.feat_names\\s*=\\s*{$")
    pattern_data = re.compile("^\\.(cpuid|msr).*$")
    pattern_end = re.compile("^},?$")

    state = state_waiting_for_type
    for line in lines:
        if state == state_waiting_for_type:
            match = pattern_type.match(line)
            if match:
                data_names = list()
                data_query = list()
                state = state_waiting_for_names

        elif state == state_waiting_for_names:
            # special case for missing ".feat_names" entry:
            match = pattern_data.match(line)
            if match:
                data_query.append(line)
                state = state_read_query
                continue

            match = pattern_names.match(line)
            if match:
                state = state_read_names

        elif state == state_read_names:
            match = pattern_end.match(line)
            if match:
                state = state_waiting_for_query
            else:
                data_names.append(line)

        elif state == state_waiting_for_query:
            match = pattern_data.match(line)
            if match:
                data_query.append(line)
                state = state_read_query

        elif state == state_read_query:
            match = pattern_end.match(line)
            data_query.append(line)
            if match:
                state = state_waiting_for_type
                data = [n.strip() for n in "".join(data_names).split(",")]
                yield data_query, data

        else:
            exit("parsing state machine in invalid state")

    if state != state_waiting_for_type:
        exit("parsing incomplete")


def qemu_extract_features(query, word, constants):
    names = dict()
    if any([e.startswith("[") for e in word]):
        for entry in word:
            entry = entry.strip()
            if not entry:
                continue
            index, name = entry.split("=", 2)
            index = int(index.strip().strip("[").strip("]"), 0)
            names[index] = name.strip().strip("\"")
    else:
        for index, name in enumerate(word):
            if not name or name == "NULL":
                continue
            names[index] = name.strip("\"")

    # qemu defines some empty feature words, filter them out
    if not names:
        return
    if all([e is None for e in names.values()]):
        return

    # cut out part between "{" and "}". easiest way to get rid of unwanted
    # extra info such as ".tcg_features" or multi line comments
    query = "".join(query).split("{")[1].split("}")[0]

    eax_in = None
    ecx_in = pycpuinfo.x86.CPUINFO_X86_CPUID_ECX_NONE
    reg = None
    msr = None
    for entry in [e.strip() for e in query.split(",")]:
        if not entry:
            continue
        left, right = [e.strip() for e in entry.split("=", 2)]
        if left == ".eax":
            eax_in = int(constants.get(right, right), 0)
        if left == ".ecx":
            ecx_in = int(constants.get(right, right), 0)
        if left == ".reg":
            reg = right.lower()[2:]
        if left == ".index":
            msr = int(constants.get(right, right), 0)

    for bit, name in sorted(names.items()):
        val = 1 << bit
        if msr:
            yield name, (msr, val & 0xFFFFFFFF, val >> 32)
        else:
            eax = 0 if reg != "eax" else val
            ebx = 0 if reg != "ebx" else val
            ecx = 0 if reg != "ecx" else val
            edx = 0 if reg != "edx" else val
            yield name, (eax_in, ecx_in, eax, ebx, ecx, edx)


def load_qemu(basedir):
    constants = qemu_get_constants(basedir)
    lines = qemu_parse_file(basedir)
    featurewords = qemu_parse_lines(lines)

    features = dict()
    for query, word in featurewords:
        for name, data in qemu_extract_features(query, word, constants):
            feature = pycpuinfo.Feature.find(name, "qemu")
            if feature:
                name = feature.name()
            features[name] = data
    return features


def compare(features):
    all_names = set()
    for feats in features.values():
        all_names = all_names.union(feats.keys())

    differences = dict()
    for name in sorted(all_names):
        availability = dict()
        for project, feats in features.items():
            availability[project] = feats.get(name)
        if len(set(availability.values())) == 1:
            continue
        differences[name] = availability
    return differences


def apply_quirks(features):
    ignore_features = {
        "libvirt": (
            "ace2",
            "ace2-en",
            "full-width-write",
            "kvm-asyncpf",
            "kvm-asyncpf-int",
            "kvm-asyncpf-vmexit",
            "kvm-hint-dedicated",
            "kvm-mmu",
            "kvm-msi-ext-dest-id",
            "kvm-nopiodelay",
            "kvm-poll-control",
            "kvm-pv-eoi",
            "kvm-pv-ipi",
            "kvm-pv-sched-yield",
            "kvm-pv-tlb-flush",
            "kvm-pv-unhalt",
            "kvm-steal-time",
            "kvmclock",
            "kvmclock-stable-bit",
            "kvmclock2",
            "phe",
            "phe-en",
            "pmm",
            "pmm-en",
            "xcrypt",
            "xcrypt-en",
            "xstore",
            "xstore-en",
        ),
        "qemu": (
            "cmt",
            "cvt16",
            "kvmclock",
            "kvmclock2",
            "mbm_local",
            "mbm_total",
            "ospke",
            "osxsave",
            "pconfig",
            "vmx-ept-uc",
            "vmx-ept-wb",
            "vmx-invept-single-context",
            "vmx-invvpid-single-context",
        )
    }

    for project, feats in ignore_features.items():
        if project not in features:
            continue
        for f in feats:
            features[project][f] = features["libcpuinfo"][f]


def main():
    parser = argparse.ArgumentParser(
        description="Compare knowledge about x86 features betweend projects")
    parser.add_argument(
        "--libvirt",
        help="Path to libvirt source code",
        type=os.path.realpath)
    parser.add_argument(
        "--qemu",
        help="Path to qemu source code",
        type=os.path.realpath)
    args = parser.parse_args()

    features = dict()
    features["libcpuinfo"] = load_libcpuinfo()

    if args.libvirt:
        features["libvirt"] = load_libvirt(args.libvirt)
    if args.qemu:
        features["qemu"] = load_qemu(args.qemu)

    if len(features) == 1:
        print("missing argument")
        exit(1)

    apply_quirks(features)
    differences = compare(features)
    if differences:
        print(json.dumps(differences, indent=2, sort_keys=True))
        exit(1)


if __name__ == "__main__":
    main()
